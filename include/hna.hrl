-ifndef (HNA_HRL).
-define (HNA_HRL, true).

-define (HNA_DEFAULT_IDLE_TIME, 5 * 60 * 1000). % default idle time between fetching the HN posts.
-define (HNA_DEFAULT_POSTS_COUNT, 50).          % default number of posts to be fetched
-define (HNA_DEFAULT_HTTP_CLIENT, httpc).       % default client module for fetching data over HTTP
-define (HNA_DEFAULT_PAGE_SIZE, 10).                   % default number of posts to be served for a single page.
-define (GPROC_KEY, {hna_fetcher, ha_posts}).          % gpproc key for subscription on posts data
-define (HNA_DEFAULT_FETCHING_IMP, hna_parallel_fetching).     % default fetching posts strategy

-endif.
