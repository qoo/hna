%%%m @doc This is WebSocket Cowboy handler.
-module (hna_ws_request_handler).

-export ([ init/2
         , websocket_init/1
         , websocket_handle/2
         , websocket_info/2
         ]).

init (Request, State) ->
    {cowboy_websocket, Request, {[], State}}.

%% @doc Subscribes to the fetcher process for posts.
websocket_init (State) ->
    ok = hna_aggregator:subscribe(self()),
    {ok, State}.

websocket_handle ({text, _}, {[], S = [Storage|_]}) ->
    %% in case we have still no posts (maybe this is the first websocket opened)
    %% we'll try to read the posts directly from the storage.
    Accessor = hna_aggregator:get_accessor(),
    Posts = Accessor(Storage, 1, all),
    {reply, generate_json(Posts), {Posts, S}};
websocket_handle ({text, _}, State = {Posts, _}) ->
    {reply, generate_json(Posts), State};
websocket_handle (_Frame, State) ->
    {ok, State}.

%% @doc Accept the fetcher subscription notification and save the posts.
websocket_info ({{hna_aggregator, ha_posts}, Posts}, State) ->
    {_, Rest} = State,
    {ok, {Posts, Rest}};
websocket_info (Msg, State) ->
    error_logger:error_report([{module, ?MODULE}, {unexpected_info, Msg}]),
    {ok, State}.

generate_json (Posts) ->
    {text, <<"[", (binary:list_to_bin(lists:join(<<",">>, Posts)))/binary , "]">>}.
