-ifndef (HNA_HTTP_HRL).
-define (HNA_HTTP_HRL, true).

-define (HNA_HTTP_DISPATCH, [{'_', [{"/[:page]", hna_http_request_handler, []}]}]).
-define (HNA_HTTP_OPTIONS, [{port, 8080}]).
-define (HNA_WS_DISPATCH, [{'_', [{"/ws", hna_ws_request_handler, []}]}]).
-define (HNA_WS_OPTIONS, [{port, 9999}]).

-endif.
