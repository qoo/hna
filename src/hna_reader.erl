%%% @doc Reads (using HN API) most top news.

-module (hna_reader).
-export ([ read_topmost_items/2
         , read_topmost_items/3
         , read_item/2
         , read_item/3
         , demo/1
         ]).

-type hn_item_id () :: non_neg_integer().
-type hn_items () :: [hn_item_id()].
-export_type ([ hn_item_id/0
              , hn_items/0
              ]).

%% @doc Returns up to `Limit' most top items of HackerNews.
%% It does a HTTP request using `HttpClient' module. The URL is taken
%% from the application environment (`hn_items_url').
%% @see read_topmost_items/3
-spec read_topmost_items (module(), pos_integer()) -> {ok, hn_items()} | {error, any()}.
read_topmost_items (HttpClient, Limit) when is_atom(HttpClient)
                                            , HttpClient =/= undefined
                                            , is_integer(Limit)
                                            , Limit > 0 ->
    Url = hna:get_env(hn_items_url),
    read_topmost_items(HttpClient, Url, Limit).

-spec read_topmost_items (module(), binary(), pos_integer()) -> {ok, hn_items()} | {error, any()}.
read_topmost_items (HttpClient, Url, Limit) when is_atom(HttpClient)
                                                 , HttpClient =/= undefined
                                                 , is_binary(Url)
                                                 , is_integer(Limit)
                                                 , Limit > 0 ->
    Response = HttpClient:request(Url),
    MaybeBody = process_response(Response),
    process_items_body(MaybeBody, Limit).

%% @doc Returns an item with given `ItemId'.
%% It does a HTTP request using `HttpClient' module. The URL is taken
%% from the application environment (`hn_item_url').
%% @see read_topmost_items/3
-spec read_item (module(), hn_item_id()) -> {ok, binary()} | {error, any()}.
read_item (HttpClient, ItemId) when is_atom(HttpClient)
                                    , HttpClient =/= undefined
                                    , is_integer(ItemId)
                                    , ItemId > -1 ->
    Url = hna:get_env(hn_item_url),
    read_item(HttpClient, Url, ItemId).

-spec read_item (module(), binary(), hn_item_id()) -> {error, any()} | {ok, binary()}.
read_item (HttpClient, Url, ItemId) when is_atom(HttpClient)
                                         , HttpClient =/= undefined
                                         , (is_binary(Url) orelse is_list(Url))
                                         , is_integer(ItemId)
                                         , ItemId > -1 ->
    ItemUrl = binary:replace(Url, <<"{id}">>, erlang:integer_to_binary(ItemId)),
    Response = HttpClient:request(ItemUrl),
    MaybeBody = process_response(Response),
    validate_item_body(MaybeBody).

%%% ------------------------------ private stuff -------------------------------------------

-spec validate_item_body ({ok, body()}) -> {ok, binary()} | {error, any()}.
validate_item_body ({ok, Body}) when is_list(Body) ->
    {ok, erlang:list_to_binary(Body)};
validate_item_body ({ok, Body}) when is_binary(Body) ->
    {ok, Body};
validate_item_body (Error = {error, _}) ->
    Error.

-type body () :: string() | binary().
-spec process_items_body ({ok, body()}, pos_integer()) -> {ok, hn_items()} | {error, any()}.
process_items_body ({ok, Body}, Limit) ->
    MaybeConvert = fun (S) when is_list(S) -> erlang:list_to_binary(S);
                       (S) when is_binary(S) -> S
                   end,
    case jsone:decode(MaybeConvert(Body)) of
        Items when is_list(Items) ->
            {ok, lists:sublist(Items, Limit)};
        _ ->
            {error, not_a_list}
    end;
process_items_body (Error = {error, _}, _) ->
    Error.

-type ok_response () :: {ok, {{any(), pos_integer(), any()}, list(), body()}}.
-type bad_response () :: {error, any()}.

-spec process_response (ok_response() | bad_response()) -> {ok, body()} | {error, any()}.
process_response ({ok, {{_, 200, _}, _Headers, Body}}) ->
    {ok, Body};
process_response (Error = {error, _}) ->
    Error.

%%% ---------------------------- demos -------------------------------

demo (items) ->
    do_demo(fun () -> read_topmost_items(httpc, 50) end);
demo (item) ->
    case read_topmost_items(httpc, 1) of
        {ok, [Item]} ->
            do_demo(fun () -> read_item(httpc, Item) end);
        {ok, _} ->
            no_items;
        Error ->
            Error
    end.

do_demo (Fun) ->
    {Time, Result} = timer:tc(Fun),
    io:format("*** The operation took ~p seconds.~n", [Time/1000000]),
    Result.
