%%% @doc Common functionality for request handlers.
-module (hna_request_handler).
-export ([ start/4
         , stop/1
         ]).

-spec start (atom(), hna_aggregator:storage(), any(), [{atom(), any()}]) -> {ok, any()}.
start (Name, Storage, DispatchPattern, Options) ->
    [{P, [{Path, Handler, Opts}]}] = DispatchPattern,
    DispatchPatternWithStorage = [{P, [{Path, Handler, [Storage | Opts]}]}],
    Dispatch = cowboy_router:compile(DispatchPatternWithStorage),
    Env = #{env => #{ dispatch => Dispatch }},
    cowboy:start_clear(Name, Options, Env).

-spec stop (any()) -> ok.
stop (RequestHandler) ->
    cowboy:stop_listener(RequestHandler).
