%%% @doc This handles HTTP request using a number of web frameworks.
%%% So far Cowboy is utulized.
-module (hna_http_request_handler).
-export ([ init/2 ]).
-export ([ content_types_provided/2 ]).
-export ([ to_json/2 ]).

-include_lib ("hna/include/hna.hrl").

init (Request, State) ->
    {cowboy_rest, Request, State}.

content_types_provided (Request, State) ->
    { [{{<<"application">>, <<"json">>, '*'}, to_json}], Request, State }.

to_json (Request, State = [Storage|_]) ->
    PageNo = binary_to_integer(cowboy_req:binding(page, Request, <<"0">>)),
    Accessor = hna_aggregator:get_accessor(),
    {Start, N} = if PageNo =< 0 ->
                         {1, all}
                   ; true ->
                         {1 + (PageNo - 1) * ?HNA_DEFAULT_PAGE_SIZE, ?HNA_DEFAULT_PAGE_SIZE}
                 end,
    Posts = Accessor(Storage, Start, N),
    { <<"[", (binary:list_to_bin(lists:join(<<",">>, Posts)))/binary , "]">>, Request, State }.
