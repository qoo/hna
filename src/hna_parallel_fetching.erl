%%% @doc Implementation of parallel strategy to fetch all of the top posts into ETS table.
%%% 1. Read specified number of topmost news ids.
%%% 2. Loop over the list of ids, spawn a small process reading their JSON representation.
%%% 3. Collect the reader result into a hash-map.
%%% 3. Store the representations into single entry ETS table ordered as the initial list of ids.

-module (hna_parallel_fetching).
-export ([ start/3 ]).
-export ([ fetch_posts/2 ]).

%% @doc Starts alone process which reads HN top news.
-spec start (hna_aggregator:posts_count(), hna_aggregator:storer(), hna_aggregator:notifier()) -> {pid(), reference()}.
start (PostsCount, Store, Notify) when is_integer(PostsCount)
                                       , PostsCount > 0
                                       , is_function(Store)
                                       , is_function(Notify) ->
    HttpClient = hna:get_env(http_client),
    erlang:spawn_monitor(fun () -> fetching_process(HttpClient, PostsCount, Store, Notify) end).

-spec fetching_process (module(), hna_aggregator:posts_count(), hna_aggregator:storer(), hna_aggregator:notifier()) -> ok.
fetching_process (HttpClient, PostsCount, Store, Notify) ->
    Items = hna_simple_fetching:fetch_items(HttpClient, PostsCount),
    Posts = fetch_posts(HttpClient, Items),
    hna_simple_fetching:store_posts(Posts, Store, Notify).

%% @doc Returns list of posts using given `HttpClient' module and list of post ids.
%% Internally it spawns `length(Itmes)' prcesses which reads post data.
-spec fetch_posts (module(), list()) -> list().
fetch_posts (HttpClient, Items) ->
    Self = self(),
    Collect = fun Loop ([], Acc) ->
                      Acc;
                  Loop (List, Acc) ->
                      receive
                          {'DOWN', Ref, process, Pid, _} ->
                              Loop(List -- [{Pid, Ref}], Acc);
                          {post, ItemId, Post} ->
                              Loop(List, Acc#{ ItemId => Post });
                          Else ->
                              io:format("unkown message ~p", [Else])
                      end
              end,
    PidsAndRefs = [ erlang:spawn_monitor(fun () ->
                                                 case hna_reader:read_item(HttpClient, ItemId) of
                                                     {ok, Post} ->
                                                         Self ! {post, ItemId, Post};
                                                     _ ->
                                                         ok
                                                 end
                                         end) || ItemId <- Items ],
    {ReadTime, Collected} = timer:tc(Collect, [PidsAndRefs, #{}]),
    Posts = lists:foldl(fun (ItemId, Acc) ->
                                case maps:find(ItemId, Collected) of
                                    {ok, Value} -> [Value|Acc];
                                    error -> Acc
                                end
                        end, [], Items),
    error_logger:info_report([ {module, ?MODULE}
                             , {posts_read, length(Posts)}
                             , {read_time, ReadTime}
                             ]),
    lists:reverse(Posts).
