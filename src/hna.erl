%%% @doc Public commn API for HNA functions.
-module (hna).
-export ([ start/0
         , stop/0
         , get_env/1
         , get_env/2
         , set_env/2
         , default_env/0
         ]).

-include_lib ("hna/include/hna.hrl").
-include ("hn_reader.hrl").
-include ("http.hrl").

%% @doc Starts HNA application.
%% @see hna_app:start/3
start () ->
    application:ensure_all_started(hna).

%% @doc Stops HNA application.
%% @see hna_app:stop/1
stop () ->
    ok = application:stop(hna),
    ok = application:stop(cowboy),
    ok = application:stop(ranch).

-type validator () :: fun((any()) -> boolean()). % Type of function validating env parameter value.
-type env_var () :: 'fetching_imp'
              | 'hn_item_url'
              | 'hn_items_url'
              | 'http_client'
              | 'http_dispatch'
              | 'http_options'
              | 'idle_time'
              | 'posts_count'
              | 'ws_dispatch'
              | 'ws_options'. % Type of env cariable/parameter name.
-type env_item () :: {idle_time, hna_aggregator:idle_time(), validator()}
                   | {posts_count, hna_aggregator:posts_count(), validator()}
                   | {http_client, module(), validator()}
                   | {hn_item_url, binary(), validator()}
                   | {hn_items_url, binary(), validator()}
                   | {http_dispatch, cowboy_router:dispatch_rules(), validator()}
                   | {http_options, [{atom(), any()}], validator()}
                   | {ws_dispatch, cowboy_router:dispatch_rules(), validator()}
                   | {ws_options, [{atom(), any()}], validator()}
                   | {fetching_imp, module(), validator()}. % type of env paramater specification.

%% @doc Returns default environment for the application.
%% This is a list of 3-element tuple: `{ParamName, DefaultValue, Validator}'
%% where `ParamName' is an atom, `DefaultValue' is any term and `Validator' is
%% a fun `(any()) -> boolean()' which for `DefaultValue' always returns `true'.
-spec default_env () -> [env_item()].
default_env () ->
    [ { idle_time, ?HNA_DEFAULT_IDLE_TIME, is_not_less_then(0) }
    , { posts_count, ?HNA_DEFAULT_POSTS_COUNT, is_not_less_then(1) }
    , { http_client, ?HNA_DEFAULT_HTTP_CLIENT, fun is_module/1 }
    , { hn_item_url, ?HN_ITEM_URL, fun is_binary/1 }
    , { hn_items_url, ?HN_ITEMS_URL, fun is_binary/1 }
    , { http_dispatch, ?HNA_HTTP_DISPATCH, fun is_dispatch_pattern/1 }
    , { http_options, ?HNA_HTTP_OPTIONS, fun is_key_value_list/1 }
    , { ws_dispatch, ?HNA_WS_DISPATCH, fun is_dispatch_pattern/1 }
    , { ws_options, ?HNA_WS_OPTIONS, fun is_key_value_list/1 }
    , { fetching_imp, ?HNA_DEFAULT_FETCHING_IMP, fun is_module/1 }
    ].

%% @doc Returns environment parameter value.
%% It may returns parameter defined in the system config file
%% or a default one. It raises an error `badarg' iff the parameter
%% is not recognized or the it's value is invalid.
-spec get_env (env_var()) -> any() | no_return().
get_env (Param) when is_atom(Param) ->
    use_env(Param, fun (DefaultValue, Validator) ->
                           get_env_value(Param, DefaultValue, Validator)
                   end).

%% @doc Returns environment parameter value optionally using `DefaultValue'.
%% The `DefaultValue' must be valid (a validator is used).
%% It may returns parameter defined in the system config file
%% or a default one. It raises an error `badarg' iff the parameter
%% is not recognized or the it's value is invalid.
-spec get_env (env_var(), any()) -> any() | no_return().
get_env (Param, DefaultValue) when is_atom(Param) ->
    use_env(Param, fun (_, Validator) ->
                           get_env_value(Param, DefaultValue, Validator)
                   end).

%% @doc Sets environment parameter.
%% Returns `ok' iff the provided `Value' is valid for given `Param'.
%% Raise `badarg' error iff `Param' is not recognized or
%% given `Value' is not valid.
-spec set_env (atom(), any()) -> ok | no_return().
set_env (Param, Value) when is_atom(Param) ->
    use_env(Param,
            fun (_, Validator) ->
                    case Validator(Value) of
                        true ->
                            application:set_env(hna, Param, Value);
                        false ->
                            error(badarg)
                    end
            end).

%%% ----------------------- private stuff ------------------------------

use_env (Param, Fun) ->
    case lists:keysearch(Param, 1, default_env()) of
        false ->
            error(badarg);
        {value, {Param, DefaultValue, Validator}} ->
            Fun(DefaultValue, Validator)
    end.

get_env_value (Param, DefaultValue, Validator) ->
    Value = application:get_env(hna, Param, DefaultValue),
    case Validator(Value) of
        true ->
            Value;
        false ->
            error(badarg)
    end.

is_module(M) ->
    M =/= undefined andalso is_atom(M).

is_not_less_then(Limit) ->
    fun (X) -> is_integer(X) andalso X >= Limit end.

is_dispatch_pattern([]) ->
    true;
is_dispatch_pattern([{_, Items} | Tail]) ->
    lists:all(fun ({Path, Module, Opts}) when is_list(Path), is_atom(Module), is_list(Opts) ->
                      true;
                  (_)->
                      false
              end, Items) andalso is_dispatch_pattern(Tail);
is_dispatch_pattern(_) ->
    false.

is_key_value_list ([]) ->
    true;
is_key_value_list ([{K, _} | Rest]) ->
    is_atom(K) andalso is_key_value_list(Rest);
is_key_value_list (_) ->
    false.
