%%% @doc Default implementation of HN posts fetcher process.
%%% This is gen_server callbacks.
-module (hna_aggregator_srv).
-behaviour (gen_server).

-export ([ init/1
         , handle_call/3
         , handle_cast/2
         , handle_info/2
         , terminate/2
         ]).

-include ("../include/hna.hrl").

-record (state, { posts_count = ?HNA_DEFAULT_POSTS_COUNT :: hna_aggregator:posts_count()
                , idle_time   = ?HNA_DEFAULT_IDLE_TIME   :: hna_aggregator:idle_time()
                , paused      = false                    :: boolean()
                , http_client = ?HNA_DEFAULT_HTTP_CLIENT :: module()
                , fetch_ref   = undefined                :: undefined | reference()
                , fetch_pid   = undefined                :: undefined | pid()
                , storage                                :: hna_aggregator:storage()
                }).

-type state () :: #state{}.

-ifdef (TESTED).
-export ([state_values/1, state_values/2, set_state/3]).
state_values (State = #state{}) ->
    Keys = record_info(fields, state),
    lists:zip(Keys, tl(tuple_to_list(State))).

state_values (Keys, State = #state{}) when is_list(Keys) ->
    Values = state_values(State),
    [ proplists:get_value(Key, Values) || Key <- Keys ].

set_state (Field, Value, State = #state{}) ->
    Fields = record_info(fields, state),
    {_, NewState} = lists:foldl(fun (F, {Counter, S}) when F =:= Field ->
                                        {Counter + 1, setelement(Counter, S, Value)};
                                    (_, {Counter, S}) ->
                                        {Counter + 1, S}
                                end, {2, State}, Fields),
    NewState.
-endif.

%% @doc Init HN fetcher process.
%% @see hna_aggregator:start_link/1
%% @see hna_aggregator:start/1
-spec init (hna_aggregator:start_args()) -> {ok, state(), timeout()}.
init ({IdleTime, PostsCount, HttpClient, Storage}) ->
    State = init_state(IdleTime, PostsCount, HttpClient, Storage),
    {ok, State, 0}.

handle_call (stop, _, State) ->
    {stop, normal, ok, State};
handle_call (get_posts, _, State) ->
    Posts = get_posts(State),
    {reply, Posts, State};
handle_call (_, _, State) ->
    {reply, ok, State}.

handle_cast (stop, State) ->
    {stop, normal, State};
handle_cast (pause, State) ->
    {noreply, pause(State)};
handle_cast (resume, State) ->
    {noreply, resume(State)}.

handle_info (timeout, State) ->
    %% initial fetch
    {noreply, fetch(State)};
handle_info (fetch, State) ->
    {noreply, fetch(State)};
handle_info ({'DOWN', _, process, Pid, _}, State = #state{ fetch_pid = Pid }) ->
    {noreply, fetching_process_terminated(State)};
handle_info (Msg, State) ->
    error_logger:info_report([{module, ?MODULE}, {unknown_info, Msg}]),
    {noreply, State}.

terminate (_Reason, State) ->
    #state{ fetch_pid = FetchPid
          , fetch_ref = FetchRef } = State,
    maybe_cancel_timer(FetchRef),
    maybe_kill_proc(FetchPid).

%%% -------------------------------------- private stuff ----------------------------------------------------

init_state (IdleTime, PostsCount, HttpClient, Storage) when is_integer(IdleTime), IdleTime > -1
                                                            , is_integer(PostsCount), PostsCount > -1
                                                            , is_atom(HttpClient)
                                                            , HttpClient =/= undefined ->
    #state{ posts_count = PostsCount
          , idle_time = IdleTime
          , http_client = HttpClient
          , storage = Storage
          }.

%% @private
%% @doc Returns stored posts.
-spec get_posts (state()) -> list().
get_posts (State) ->
    case ets:lookup(State#state.storage, posts) of
                [{posts, Ps}] -> Ps;
        _ -> []
    end.

%% @private
%% @doc Pause fetching.
%% Effects in killing fetching timer.
%% The on-going fetching process is still working.
-spec pause (state()) -> state().
pause (State) ->
    maybe_cancel_timer(State#state.fetch_ref),
    State#state{ paused = true, fetch_ref = undefined }.

%% @private
%% @doc Resume fetching (iff paused).
%% Effects in starting new fetch.
-spec resume (state()) -> state().
resume (State = #state{ paused = false}) ->
    State;
resume (State = #state{ paused = true}) ->
    fetch(State#state{ paused = false }).

%% @doc Acknowledge that fetching process has been terminated
fetching_process_terminated (State = #state{}) ->
    State#state{ fetch_pid = undefined }.

%% @private
%% @doc Starts fetching HN posts.
%% 1. Spawns a reader process.
%% 2. Fires up a timer to wake up after `idle_time'.
-spec fetch (state()) -> state().
fetch (State) ->
    State1 = spawn_reader(State),
    IdleTime = State#state.idle_time,
    State1#state{ fetch_ref = erlang:send_after(IdleTime, self(), fetch) }.

%% @private
%% @doc Spawns a process retrieving posts from HN.
%% The process is not linked but monitored. If a previously
%% reading process is still alive it will be killled.
spawn_reader (State) ->
    #state{ fetch_pid = FetchPid
          , posts_count = PostsCount
          , storage = Table } = State,
    ok = maybe_kill_proc(FetchPid),
    Fetching = hna:get_env(fetching_imp),
    {Pid, _} = Fetching:start(PostsCount,
                              fun (Posts) -> store(Table, Posts) end,
                              fun hna_aggregator:notify/1),
    State#state{ fetch_pid = Pid }.

%% @doc Stores given `Posts' into a memory cache.
-spec store (ets:tab(), list()) -> ok.
store (Storage, Posts) ->
    true = ets:delete_all_objects(Storage),
    true = ets:insert(Storage, {posts, Posts}),
    error_logger:info_report([{module, ?MODULE}, {posts_stored, length(Posts)}]),
    ok.

maybe_cancel_timer (undefined) ->
    ok;
maybe_cancel_timer (TimerRef) ->
    _ = erlang:cancel_timer(TimerRef),
    ok.

maybe_kill_proc (Pid) when is_pid(Pid) ->
    exit(Pid, kill),
    receive
        {'DOWN', _, process, Pid, _} ->
            ok
    after 1000 ->
            {zombie_process, Pid}
    end;
maybe_kill_proc (_) ->
    ok.
