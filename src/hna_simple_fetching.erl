%%% @doc Implementation of simple strategy to fetch all of the top posts into ETS table.
%%% It is just dumb simple:
%%% 1. Read specified number of topmost news ids.
%%% 2. Loop over the list of ids and read their JSON representation.
%%% 3. Store the representations into single entry ETS table.

-module (hna_simple_fetching).
-export ([ start/3 ]).
-export ([ fetch_items/2
         , fetch_posts/2
         , store_posts/3
         ]).

%% @doc Starts alone process which reads HN top news.
-spec start (hna_aggregator:posts_count(), hna_aggregator:storer(), hna_aggregator:notifier()) -> {pid(), reference()}.
start (PostsCount, Store, Notify) when is_integer(PostsCount)
                                       , PostsCount > 0
                                       , is_function(Store)
                                       , is_function(Notify) ->
    erlang:spawn_monitor(fun () ->
                                 HttpClient = hna:get_env(http_client),
                                 Items = fetch_items(HttpClient, PostsCount),
                                 Posts = fetch_posts(HttpClient, Items),
                                 store_posts(Posts, Store, Notify)
                         end).

%% @doc Returns list of top `N' posts ids using given `HttpClient' module.
-spec fetch_items (module(), hna_aggregator:posts_count()) -> hna_reader:hn_items().
fetch_items (HttpClient, N) when is_atom(HttpClient)
                                 , HttpClient =/= undefined
                                 , is_integer(N)
                                 , N > -1 ->
    error_logger:info_report(reader_spawned),
    {ok, Items} = hna_reader:read_topmost_items(HttpClient, N),
    error_logger:info_report([{module, ?MODULE}, {items_read, length(Items)}]),
    Items.

%% @doc Returns list of posts using given `HttpClient' module and list of post ids.
-spec fetch_posts (module(), list()) -> list().
fetch_posts (HttpClient, Items) ->
    {ReadTime, Posts} = timer:tc(fun collect_posts/2, [HttpClient, Items]),
    error_logger:info_report([ {module, ?MODULE}
                             , {posts_read, length(Posts)}
                             , {read_time, ReadTime}
                             ]),
    Posts.

collect_posts (HttpClient, Items) ->
    lists:reverse(lists:foldl(fun (ItemId, Posts) ->
                                      case hna_reader:read_item(HttpClient, ItemId) of
                                          {ok, Post} -> [Post|Posts];
                                          _Error -> Posts
                                      end
                              end, [], Items)).

%%% @doc Stores posts and notifies about them.
-spec store_posts (list(), hna_aggregator:storer(), hna_aggregator:notifier()) -> ok.
store_posts (Posts, Store, Notify) ->
    ok = Store(Posts),
    ok = Notify(Posts),
    error_logger:info_report([{module, ?MODULE}, {notification, ok}]).
