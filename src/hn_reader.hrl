-ifndef (HN_READER_HRL).
-define (HN_READER_HRL, true).

-define (HN_ITEMS_URL, <<"https://hacker-news.firebaseio.com/v0/topstories.json">>).
-define (HN_ITEM_URL, <<"https://hacker-news.firebaseio.com/v0/item/{id}.json">>).

-endif.
