-module (hna_sup).

-behaviour (supervisor).

-export ([start_link/1]).

-export ([init/1]).

-define (SERVER, ?MODULE).

start_link(Storage) ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, [Storage]).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init([Storage]) ->
    SupFlags = #{ strategy => one_for_all
                , intensity => 100
                , period => 1
                },
    HnaAggregatorSpec = #{ id => hna_aggregator
                         , start => { hna_aggregator, start_link, [Storage] }
                         , type => worker
                         , modules => [ hna_aggregator_srv ]
                         },
    ChildSpecs = [ HnaAggregatorSpec ],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
