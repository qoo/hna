%%% @doc Public API for HN top news fetcher.
-module (hna_aggregator).

-export ([ start/1
         , start_link/1
         , stop/0
         , async_stop/0
         , get_posts/0
         , get_accessor/0
         , init_storage/0
         , destroy_storage/1
         , subscribe/1
         , notify/1
         ]).

-type posts_count () :: pos_integer().    % Type of number of posts being fetched.
-type idle_time () :: non_neg_integer().  % Type of idle time between fetch tries.
-type proto_calls () :: stop
                      | get_posts.        % Type of synchronous calls.
-type proto_casts () :: stop
                      | pause
                      | resume.           % Type of gen_server cast messages.
-type proto_infos () :: fetch.            % Type of gen_server out-of-protocol messages.
-type proto () :: proto_calls()
                | proto_casts()
                | proto_infos().          % Type of gen_server messages.
-type storage () :: ets:tab().            % Type of storage reference.
-type accessor () ::
        fun((Storage :: storage()
            , From :: pos_integer()
            , N :: non_neg_integer()
                 | all) -> [any()]).      % Type of function accessing stored posts.

-type notifier () :: fun((any()) -> ok).  % Type of function notifying about posts just fetched.
-type storer () :: fun((list()) -> ok).   % Type of function storing list of posts into a cache.

-type start_args () :: {idle_time(), posts_count(), module(), storage()}.
 % Type of list of arguments for starting aggregator worker.

-export_type ([ start_args/0
              , posts_count/0
              , idle_time/0
              , proto_calls/0
              , proto_casts/0
              , proto_infos/0
              , proto/0
              , storage/0
              , accessor/0
              , storer/0
              , notifier/0
              ]).

-include_lib ("hna/include/hna.hrl").

%% @doc Returns brand new initialized storage for posts.
-spec init_storage () -> storage().
init_storage () ->
    ets:new(hna_input_table, [named_table, {write_concurrency, false}, public]).

%% @doc Removes all traces of the posts cache.
-spec destroy_storage (storage()) -> true.
destroy_storage (Storage) ->
    ets:delete(Storage).

%% @doc Start unsupervised HN fetcher process.
%% Regarded as develpment only.
-spec start (storage()) -> pid().
start (Storage) ->
    start(start, Storage).

%% @doc Start linked HN fetcher process.
%% The configuration parameters may be read by to `start/1' or `start_link/1'
%% and/or defined in the system config file.
%% <ul>
%% <li>`idle_time' - time between consecutive fetches</li>
%% <li>`posts_count' - count of posts to be fetched</li>
%% <li>`http_client' - HTTP client communication implementation module</li>
%% </ul>
%% @see start/1
-spec start_link (storage()) -> pid().
start_link (Storage) ->
    start(start_link, Storage).

%% @doc Stop HN fetcher process synchronically.
-spec stop () -> ok.
stop () ->
    send(call, stop).

%% @doc Stop HN fetcher process asynchronically.
-spec async_stop () -> ok.
async_stop () ->
    send(cast, stop).

%% @private
%% @doc Helper function to start HN fetcher process.
-spec start (start | start_link, storage()) -> pid().
start (GenFun, Storage) ->
    IdleTime = hna:get_env(idle_time),
    PostsCount = hna:get_env(posts_count),
    HttpClient = hna:get_env(http_client),
    gen_server:GenFun({local, ?MODULE}, hna_aggregator_srv, {IdleTime, PostsCount, HttpClient, Storage}, []).

%% @doc Returns storage handler.
-spec get_posts () -> storage().
get_posts () ->
    send(call, get_posts).

%% @doc Returns function accessing stored posts.
-spec get_accessor () -> accessor().
get_accessor () ->
    fun post_accessor/3.

%% @doc Subscribes a process for receiving notifications about new posts.
-spec subscribe (pid()) -> ok.
subscribe (Receiver) when is_pid(Receiver) ->
    true = gproc:reg({p, l, ?GPROC_KEY}),
    ok.

%% @doc Notifies subscribers about new posts having been fetched.
-spec notify (any()) -> ok.
notify (Data) ->
    gproc:send({p, l, ?GPROC_KEY}, {?GPROC_KEY, Data}),
    ok.

%% @private
%% @doc Function returning list of post data from given storage.
%% Returns up to `N' items, starting from `From' item in the fetched list.
post_accessor (Storage, From, N) when is_integer(From)
                                      , From > 0
                                      , (N =:= all orelse (is_integer(N) andalso N > -1)) ->
    case ets:lookup(Storage, posts) of
        [{posts, Posts}] ->
            {Start, M} = if N =:= all ->
                                 {1, length(Posts)}
                           ; true ->
                                 {From, N}
                         end,
            lists:sublist(Posts, Start, M)
        ; _ ->
            []
    end.

%% @private
%% @doc Send message to the HN fetcher process.
-spec send (call | cast, any()) -> ok.
send (GenFun, Msg) when GenFun =:= call orelse GenFun =:= cast ->
    gen_server:GenFun(?MODULE, Msg).
