%%% @doc `hna' application module public API
%%%
-module(hna_app).

-behaviour(application).

-export ([ start/2
         , stop/1
         ]).

start (_StartType, _StartArgs) ->
    Storage = hna_aggregator:init_storage(),
    HttpDispatchPattern = hna:get_env(http_dispatch),
    HttpOptions = hna:get_env(http_options),
    WsDispatchPattern = hna:get_env(ws_dispatch),
    WsOptions = hna:get_env(ws_options),
    {ok, HttpHandler} = hna_request_handler:start(hna_http, Storage, HttpDispatchPattern, HttpOptions),
    {ok, WsHandler} = hna_request_handler:start(hna_ws, Storage, WsDispatchPattern, WsOptions),
    {ok, Pid} = hna_sup:start_link(Storage),
    {ok, Pid, {[HttpHandler, WsHandler], Storage}}.

stop ({Handlers, Storage}) ->
    [ hna_request_handler:stop(Handler) || Handler <- Handlers ],
    hna_aggregator:destroy_storage(Storage),
    ok.

%% internal functions
