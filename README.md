HNA
=====

HackerNews Aggregator.

Build
-----

    $ rebar3 get-deps
    $ rebar3 compile
    $ rebar3 dialyzer

Tests
-----

    $ rebar3 as test eunit
    $ rebar3 as test proper

Documentation
-------------

    $ rebar3 edoc

Then look into the `doc` folder.

Demos
-----

    $ rebar3 shell
    Erlang/OTP 23 [erts-11.1.5] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

    Eshell V11.1.5  (abort with ^G)
    1> hna:start().

and try to connect at <http://127.0.0.1:8080> and try HTTP API:

- <http://127.0.0.1:8080>: all aggregated prosts are presented (as JSON documents)
- <http://127.0.0.1:8080/1>: first 10 JSONs
- <http://127.0.0.1:8080/2>: next 10 JSONs
- <http://127.0.0.1:8080/3>: etc...

... you get the idea.

WebSockets may be tried using one of the available browser plugins
(for example for Chrome: Simple Websocket Client) connecting at:
<ws://127.0.0.1:9999/ws>. Sending anything through the socket results
in getting list of all aggregated posts as JSON objects.

General remarks
---------------

The application spawns single worker (`hna_aggregator`) which repeatedly
spwans anonymous monitored processs which does the dirty work of
getting HN posts and stores them into a cache.

Currently the cache is implemented as ETS table.

The application starts 2 handlers:

- HTTP handler (`hna_http_request_handler`)
- WebSockets handler (`hna_ws_request_handler`)

both of which are implemented as `cowboy` handlers.

HN posts may be read using 2 startegies:

- "simple" (`hna_simple_fetching`), which just loop over items ids and sequentially connects to HN;
- "parallel", the default, (`hna_parallel_fetching`), which reads HN posts in parallel.

One may try both scenarios and see the difference in time of reading using `simple` and `parallel` profiles respectively:

    $ rebar3 as simple shell

or

    $ rebar3 as parallel shell
