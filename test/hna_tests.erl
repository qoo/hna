-module(hna_tests).

-include_lib("eunit/include/eunit.hrl").
-include ("../src/http.hrl").

generate_env(DefaultValue) ->
    case DefaultValue of
        X when is_binary(X) ->
            binary_to_list(X);
        X when is_list(X) ->
            0;
        X when is_atom(X) ->
            undefined;
        X when is_integer(X) ->
            -1 * X;
        X when X =:= ?HNA_HTTP_DISPATCH; X =:= ?HNA_WS_DISPATCH ->
            [];
        X when X =:= ?HNA_HTTP_OPTIONS; X =:= ?HNA_WS_DISPATCH ->
            [bad_value, [], {1, 2}];
        X when is_atom(X) ->
            "atom"
    end.

'Default environment variables _test'() ->
    %% This is a "reference" of currently recognized parameters
    Recognized = [ idle_time
                 , posts_count
                 , http_client
                 , hn_items_url
                 , hn_item_url
                 , http_dispatch
                 , http_options
                 , ws_dispatch
                 , ws_options
                 , fetching_imp
                 ],
    Env = hna:default_env(),

    ?assertEqual(length(Recognized), length(Env)),

    %% all default values must be valid
    [begin
         application:unset_env(hna, Param),
         ?assertMatch({Param, true}, {Param, lists:member(Param, Recognized)}),
         ?assertMatch({Param, true}, {Param, Validator(DefaultValue)})
     end
     || {Param, DefaultValue, Validator} <- Env].

'Default environment values _test'() ->
    Env = hna:default_env(),
    %% all default values must be valid
    [begin
         application:unset_env(hna, Param),
         ?assertMatch({Param, true}, {Param, Validator(DefaultValue)})
     end
     || {Param, DefaultValue, Validator} <- Env].

'Getting environment parameter with invalid custom default value _test'() ->
    Env = hna:default_env(),
    [begin
         application:unset_env(hna, Param),
         ?assertError(badarg,
                      hna:get_env(Param, generate_env(DefaultValue)))
     end
     || {Param, DefaultValue, _} <- Env].

'Getting environment parameter with valid custom default value _test'() ->
    Env = hna:default_env(),
    [begin
         application:unset_env(hna, Param),
         ?assertMatch({Param, DefaultValue}, {Param, hna:get_env(Param, DefaultValue)})
     end
     || {Param, DefaultValue, _} <- Env].

'Setting valid environment parameter _test'() ->
    Env = hna:default_env(),
    [?assertMatch({Param, ok}, {Param, hna:set_env(Param, DefaultValue)})
     || {Param, DefaultValue, _} <- Env].

'Setting invalid environment _test'() ->
    Env = hna:default_env(),
    [?assertError(badarg,
                  hna:set_env(Param, generate_env(DefaultValue)))
     || {Param, DefaultValue, _} <- Env].
