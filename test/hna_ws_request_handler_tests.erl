-module (hna_ws_request_handler_tests).
-include_lib ("eunit/include/eunit.hrl").

-include ("../src/http.hrl").

-define (TIMEOUT, 10000).

'Simple request _test_' () ->
    Length = 50,
    SamplePostsBin = [ <<"{\"id\":", (integer_to_binary(I))/binary, "}">> || I <- lists:seq(1, Length) ],
    SamplePosts =  [ #{ <<"id">> => I } || I <- lists:seq(1, Length) ],
    Accessor = fun (_, _, all) ->
                       lists:sublist(SamplePostsBin, 1, length(SamplePostsBin));
                   (_, From, N) ->
                       lists:sublist(SamplePostsBin, From, N)
               end,
    Open = fun () ->
                   Port = proplists:get_value(port, hna:get_env(ws_options)),
                   {ok, Connection} = gun:open("127.0.0.1", Port),
                   {ok, _} = gun:await_up(Connection),
                   Connection
           end,
    UpgradeToWs = fun (Connection) ->
                         Stream = gun:ws_upgrade(Connection, "/ws"),
                         ?assertMatch({ok, _}, receive
                                                   {gun_upgrade, Connection, Stream, X, _} ->
                                                       {ok, X};
                                                   {gun_response, Connection, _, _, Status, Headers} ->
                                                       {error, {ws_upgrade_failed, Status, Headers}};
                                                   {gun_error, Connection, Stream, Reason} ->
                                                       {error, {ws_upgrade_failed, Reason}}
                                               after ?TIMEOUT ->
                                                       {error, timeout}
                                               end),
                          ok
                  end,
    SendAndReceive = fun (Connection) ->
                         gun:ws_send(Connection, {text, ""}),
                         ?assertMatch(true, receive
                                                {gun_ws, Connection, _, Frame} ->
                                                    case Frame of
                                                        {text, Body} ->
                                                            jsone:decode(Body) =:= SamplePosts;
                                                        Else ->
                                                            {error, {unexpected_reponse, Else}}
                                                    end;
                                                Other ->
                                                    {error, {unexpected_response, Other}}
                                            after ?TIMEOUT ->
                                                    {error, timeout}
                                            end)
                     end,
    Call = fun (N) ->
                   fun () ->
                           Connection = Open(),
                           try
                               ok = UpgradeToWs(Connection),
                               [ SendAndReceive(Connection) || _ <- lists:seq(1, N) ]
                           after
                               gun:close(Connection)
                           end
                   end
           end,

    %% Test cases
    SingleCall = { "Single call", Call(1) },
    MultipleClientsTest =
        fun (N) ->
                { "Multiple clients call",
                  fun () ->
                          erlang:process_flag(trap_exit, true),
                          Pids0 = [ erlang:spawn_link(Call(1)) || _ <- lists:seq(1, N) ],
                          Wait = fun Loop ([]) ->
                                         ok;
                                     Loop (Pids) ->
                                         receive
                                             {'EXIT', Pid, _} ->
                                                 Loop(Pids -- [Pid])
                                         after ?TIMEOUT ->
                                                 {error, timeout}
                                         end
                                 end,
                          %% let's wait for the client processes to finish...
                          try
                              ?assertMatch(ok, Wait(Pids0))
                          after
                              erlang:process_flag(trap_exit, false)
                          end
                  end }
        end,
    MultipleCall = fun (N) ->
                           { "Multiple call to single handler", {inorder, Call(N)} }
                   end,

    %% Main test suite.
    { setup
    , fun () ->
              meck:new(hna_aggregator, [passthrough]),
              %% no call to HN is executed.
              meck:expect(hna_simple_fetching, fetch_items, 2, []),
              meck:expect(hna_simple_fetching, fetch_posts, 2, SamplePostsBin),
              meck:expect(hna_aggregator, get_accessor, fun () -> Accessor end),
              hna:start(),
              application:ensure_all_started(gun),
              [hna_aggregator, hna_simple_fetching]
      end
    , fun (Mods) ->
              application:stop(gun),
              hna:stop(),
              meck:unload(Mods)
      end
    , {inorder, [ SingleCall
                , {timeout, 30, MultipleClientsTest(100)}
                , {timeout, 60, MultipleCall(1000)}
                ]}
    }.
