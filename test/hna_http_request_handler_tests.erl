-module (hna_http_request_handler_tests).
-include_lib ("eunit/include/eunit.hrl").

-include ("../src/http.hrl").

'Simple requests _test_' () ->
    Length = 50,
    SamplePostsBin = [ <<"{\"id\":", (integer_to_binary(I))/binary, "}">> || I <- lists:seq(1, Length) ],
    Accessor = fun (_, _, all) ->
                       lists:sublist(SamplePostsBin, 1, length(SamplePostsBin));
                   (_, From, N) ->
                       lists:sublist(SamplePostsBin, From, N)
               end,
    SendAndReceive = fun (Page) ->
                             Port = proplists:get_value(port, hna:get_env(http_options)),
                             Tail = case Page of
                                        none -> "";
                                        _ -> "/" ++ integer_to_list(Page)
                                    end,
                             httpc:request("http://127.0.0.1:" ++ integer_to_list(Port) ++ Tail)
                     end,

    %% Test cases
    Call = fun (Page, From, N) ->
                   { "Simple call for a page"
                   , fun () ->
                             Response = SendAndReceive(Page),
                             ?assertMatch({true, Page, From, N},
                                          case Response of
                                              {ok, {{_, 200, _}, _, Body}} ->
                                                  { jsone:decode(list_to_binary(Body))
                                                    =:= [ jsone:decode(Bin) || Bin <- Accessor(ignore, From, N) ]
                                                  , Page, From, N };
                                              Else ->
                                                  {error, {invalid_response, Else}}
                                          end)
                     end
                   }
           end,
    Paging =
        fun (Mode) ->
                { "Paging - " ++ atom_to_list(Mode)
                , { Mode,
                    [ { "Call for page " ++ integer_to_list(PageNo)
                      , fun () ->
                                Call(PageNo, From, N)
                        end
                      } || {PageNo, From, N} <- [ {-1, 1, Length}
                                                , {0, 1, Length}
                                                , {1, 1, 10}
                                                , {2, 11, 10}
                                                , {3, 21, 10}
                                                , {4, 31, 10}
                                                , {5, 41, 10}
                                                ]
                    ] }
                }
        end,

    %% Tests suite.
    { setup
    , fun () ->
              meck:new(hna_aggregator, [passthrough]),
              FakeProc = fun () ->
                                 receive ok -> ok end
                         end,
              %% no real call is made to HN
              meck:expect(hna_aggregator, start_link, fun (_) -> {ok, spawn(FakeProc)} end),
              meck:expect(hna_aggregator, get_accessor, fun () -> Accessor end),
              hna:start(),
              [hna_aggregator]
      end
    , fun (Mods) ->
              hna:stop(),
              meck:unload(Mods)
      end
    , [ Call(none, 1, Length) , Paging(inorder), Paging(inparallel) ]
    }.
