-module (prop_hna_aggregator_srv).

-include_lib ("proper/include/proper.hrl").
-include_lib ("eunit/include/eunit.hrl").

-include ("../src/hn_reader.hrl").

-export ([ prop_handle_call/1
         , prop_handle_cast/1
         , prop_handle_info/1
         ]).

prop_init_srv () ->
    Tab = ets:new(?MODULE, [public]),
    ?FORALL({ PostsCount, IdleTime },
            { hna_aggregator:posts_count(), hna_aggregator:idle_time() },
            begin
                Args = {IdleTime, PostsCount, httpc, Tab},
                {ok, State, Timeout} = hna_aggregator_srv:init(Args),
                [IdleTime1, PostsCount1] = hna_aggregator_srv:state_values([idle_time, posts_count], State),
                Timeout > -1
                    andalso IdleTime1 =:= IdleTime
                    andalso PostsCount1 =:= PostsCount
            end).

%% What message handlers should return?
%% These funcitons generates function which compares argument with expected handler value.

handler_returns (handle_call, stop, State) ->
    fun ({stop, normal, ok, S}) when S =:= State ->
            {true, S};
        (Other) ->
            {false, Other}
    end;
handler_returns (handle_call, get_posts, State) ->
    fun ({reply, [], S}) when S =:= State ->
            {true, S};
        (Other) ->
            {false, Other}
    end;
handler_returns (handle_cast, stop, State) ->
    fun ({stop, normal, S}) when S =:= State ->
            {true, S};
        (Other) ->
            {false, Other}
    end;
handler_returns (handle_cast, pause, State) ->
    fun ({noreply, S}) ->
            %% the timer is canceled, the fetching process is untouched
            [PrevPid] = hna_aggregator_srv:state_values([fetch_pid], State),
            [Paused, FetchRef, FetchPid] =
                hna_aggregator_srv:state_values([paused, fetch_ref, fetch_pid], S),
            { Paused
              andalso FetchRef =:= undefined
              andalso FetchPid =:= PrevPid,
              S };
        (Other) ->
            {false, Other}
    end;
handler_returns (handle_cast, resume, State) ->
    fun ({noreply, S}) ->
            %% resume fetching:
            %% 0. iff the pause is false - do nothing.
            %% otherwise:
            %% 1. pause flag is set to false.
            %% 2. tries to start fetching at once
            [PrevPaused] = hna_aggregator_srv:state_values([paused], State),
            [Paused] = hna_aggregator_srv:state_values([paused], S),
            { (PrevPaused =:= Paused)
              orelse
                (not Paused
                 %% and immediately runs fetch.
                 andalso check_fetch(State, S))
            , S };
        (Other) ->
            {false, Other}
    end;
handler_returns (handle_info, fetch, State) ->
    fun ({noreply, S}) ->
            %% a fetch timeout occured
            {check_fetch(State, S), S};
        (Other) ->
           {false, Other}
    end;
handler_returns (handle_info, {'DOWN', MRef, process, Pid, _}, State) ->
    fun ({noreply, S}) when is_reference(MRef), is_pid(Pid) ->
            %% HTTP client subprocess has finished - clear it's marker inside the state
            {hna_aggregator_srv:set_state(fetch_pid, undefined, State) =:= S, S};
        (Other) ->
            {false, Other}
    end.

check_fetch (PreviousState, CurrentState) ->
    %% when fetching is started:
    [PrevFetchRef, PrevFetchPid] = hna_aggregator_srv:state_values([fetch_ref, fetch_pid], PreviousState),
    [FetchRef, FetchPid] = hna_aggregator_srv:state_values([fetch_ref, fetch_pid], CurrentState),
    %% we have a new timer started
    PrevFetchRef =/= FetchRef
        andalso
        %% new fetchig process is spawned
        PrevFetchPid =/= FetchPid.

%% What effects are when handler is applied?

handler_effects (handle_cast, pause, State) ->
    %% if a fetching process has been working - it is still alive
    [FetchPid] = hna_aggregator_srv:state_values([fetch_pid], State),
    FetchPid =:= undefined
        orelse (is_pid(FetchPid)
                andalso erlang:is_process_alive(FetchPid));
handler_effects (handle_info, fetch, State) ->
    [FetchRef, FetchPid, IdleTime, Table, Count] =
        hna_aggregator_srv:state_values([fetch_ref, fetch_pid, idle_time, storage, posts_count],
                                     State),
    case {FetchRef, FetchPid} of
        {Ref, undefined} when is_reference(Ref) ->
            %% a fetch is already waiting to start, no effect is done
            true;
        {Ref, Pid} when is_reference(Ref), is_pid(Pid) ->
            %% a new fetch timer is ticking, a fetching subprocess is working...
            MRef = erlang:monitor(process, Pid),
            receive
                %% wait for subprocess to finish
                {'DOWN', MRef, process, Pid, _} ->
                    %% check the storage contents - should be something.
                    case ets:lookup(Table, posts) of
                        [{posts, Posts}] when Count =:= length(Posts) ->
                            true;
                        _ ->
                            false
                    end
            end,
            %% catch the exhausting timer
            receive
                fetch ->
                    true
            after IdleTime + 500 ->
                    false
            end
    end;
handler_effects (_, _, _) ->
    true.

%% Check sync handling.

prop_handle_call (doc) ->
    "Checks handling of synchronous messages.";
prop_handle_call (opts) ->
    [{numtests, 100}].

prop_handle_call () ->
    ?FORALL(Msg, hna_aggregator:proto_calls(), check_handle_msg(handle_call, Msg, [from_here])).

%% Check async handling.

prop_handle_cast (doc) ->
    "Checks handling of asynchronous messages.";
prop_handle_cast (opts) ->
    [{numtests, 100}].

prop_handle_cast () ->
    ?FORALL(Msg, hna_aggregator:proto_casts(), check_handle_msg(handle_cast, Msg, [])).

%% Check out-of-protocol handling.

prop_handle_info (doc) ->
    "Checks handling of out-of-protocol messages.";
prop_handle_info (opts) ->
    [{numtests, 100}].

hn_mock (NumberOfPosts) ->
    %% unofficial HN service mock ;)
    fun (Url) ->
            case binary:match(Url, <<"topmost.json">>) of
                nomatch ->
                    Body = jsone:encode(lists:seq(1, NumberOfPosts)),
                    %% item request
                    {ok, {{"HTTP/x", 200, "OK"}, [], Body}};
                _ ->
                    {ok, {{"HTTP/x", 200, "OK"}, [], "{}"}}
            end
    end.

prop_handle_info () ->
    NumberOfPosts = 1,
    Tab = ets:new(?MODULE, [public]),
    {ok, State, _} = hna_aggregator_srv:init({50, NumberOfPosts, httpc, Tab}),
    ?SETUP(fun () ->
                   meck:expect(gproc, reg, 1, true),
                   meck:expect(gproc, send, 2, true),
                   meck:expect(httpc, request, 1, hn_mock(NumberOfPosts)),
                   fun () ->
                           meck:unload([httpc, gproc])
                   end
           end,
           ?FORALL(Msg, hna_aggregator:proto_infos(), check_handle_msg(State, handle_info, Msg, []))).


check_handle_msg (Handling, Msg, Args) ->
    Tab = ets:new(?MODULE, [public]),
    {ok, State, _} = hna_aggregator_srv:init({1000, 1, httpc, Tab}),
    check_handle_msg(State, Handling, Msg, Args).

check_handle_msg (State, Handling, Msg, Args) ->
    Check = handler_returns(Handling, Msg, State),
    Args1 = [Msg | Args ++ [State]],
    Result = apply(hna_aggregator_srv, Handling, Args1),
    case Check(Result) of
        {true, NextState} ->
            handler_effects(Handling, Msg, NextState);
        {false, Other} ->
            ?debugFmt("Unexpected state: ~p", [hna_aggregator_srv:state_values(Other)]),
            false
    end.
